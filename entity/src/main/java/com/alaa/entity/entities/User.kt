package com.alaa.entity.entities

import android.os.Parcelable
import androidx.room.Entity
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
@Entity(primaryKeys = ["id"])
data class User(
    val id: Int,
    val email: String?,
    @Json(name = "first_name")
    var firstName: String?,
    @Json(name = "last_name")
    val lastName: String?,
    var avatar: String? = ""
):Parcelable