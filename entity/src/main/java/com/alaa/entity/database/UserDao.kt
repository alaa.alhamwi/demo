package com.alaa.entity.database

import androidx.room.*
import com.alaa.entity.entities.User

@Dao
abstract class UserDao {
    @Insert
    abstract suspend fun insertUser(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertUser(users: List<User>)

    @Query("DELETE from user")
    abstract suspend fun deleteUsers()

    @Query("SELECT * from user")
    abstract suspend fun getUsers(): List<User>

    @Transaction
    open suspend fun setUsers(users: List<User>) {
        deleteUsers()
        insertUser(users)
    }

}