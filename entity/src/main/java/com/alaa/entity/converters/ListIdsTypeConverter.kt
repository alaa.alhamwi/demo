package com.alaa.entity.converters

import androidx.room.TypeConverter

object ListIdsTypeConverter {
    @TypeConverter
    @JvmStatic
    fun stringToIntList(data: String?): List<Int>? {
        return data?.let {
            it.split(",").map { s ->
                try {
                    if (s.isEmpty())
                        null
                    else
                        s.toInt()
                } catch (ex: Throwable) {
                    null
                }
            }
        }?.filterNotNull()
    }

    @TypeConverter
    @JvmStatic
    fun intListToString(ints: List<Int>?): String? {
        return ints?.joinToString(",")
    }
}
