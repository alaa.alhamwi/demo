package com.alaa.network.util

import com.alaa.network.vo.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlin.test.assertEquals


fun <T> Flow<Resource<T>>.test(scope: CoroutineScope): FLowTest<T> {
    return FLowTest(scope, this)
}

class FLowTest<T>(
    scope: CoroutineScope,
    flow: Flow<Resource<T>>
) {
    private val values = mutableListOf<Resource<T>>()
    private val valuesToCheck = mutableListOf<Resource<T>>()
    lateinit var assertBlock: (responseValues: MutableList<Resource<T>>, assertedValues: MutableList<Resource<T>>) -> Unit
    private val job: Job = scope.launch {
        flow.collect {
            values.add(it)
        }
        delay(100)
        runAssertValues()
        delay(200)
        finish()
    }

    fun assertNoValues(): FLowTest<T> {
        @Suppress("RemoveExplicitTypeArguments")
        assertEquals(emptyList<Resource<T>>(), this.values)
        return this
    }

    fun assertValues(
        vararg values: Resource<T>,
        block: (responseValues: MutableList<Resource<T>>, assertedValues: MutableList<Resource<T>>) -> Unit
    ): FLowTest<T> {
        assertValues(*values)
        this.assertBlock = block
        return this
    }

    private fun assertValues(
        vararg values: Resource<T>
    ): FLowTest<T> {
        this.valuesToCheck.addAll(values.toList())
        return this
    }

    @Synchronized
    private fun runAssertValues(): FLowTest<T> {

        assertEquals(valuesToCheck.toList().size, this.values.size)
        assertEquals(valuesToCheck.toList(), this.values)
        this.assertBlock(values, valuesToCheck)
        return this
    }

    private fun finish() {
        job.cancel()
    }
}