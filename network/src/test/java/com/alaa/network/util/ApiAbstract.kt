package com.alaa.network.util

import com.alaa.network.resources.NetworkBoundResource
import com.alaa.network.vo.response.util.ApiResponse
import kotlinx.coroutines.flow.Flow
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.junit.runners.Parameterized
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.nio.charset.StandardCharsets

@RunWith(JUnit4::class)
abstract class ApiAbstract<T, RequestType, ResponseType> {

    lateinit var handleSaveCallResult: (ResponseType) -> Unit

    lateinit var handleShouldFetch: (RequestType?) -> Boolean

    lateinit var handleCreateCall: () -> Flow<ApiResponse<ResponseType>?>

    lateinit var handleLoadFromDb: () -> RequestType

    lateinit var networkBoundResource: NetworkBoundResource<RequestType, ResponseType>

    private lateinit var mockWebServer: MockWebServer

    @Throws(IOException::class)
    @Before
    fun mockServer() {
        mockWebServer = MockWebServer()
        mockWebServer.start()

        networkBoundResource =
            object : NetworkBoundResource<RequestType, ResponseType>() {

                override fun onFetchFailed() {

                }

                override suspend fun saveCallResult(item: ResponseType) {
                    handleSaveCallResult(item)
                }

                override fun shouldFetch(data: RequestType?): Boolean {
                    return handleShouldFetch(data)
                }

                override suspend fun loadFromDb(): RequestType {
                    return handleLoadFromDb()
                }

                override suspend fun createCall(): Flow<ApiResponse<ResponseType>?> {
                    return handleCreateCall()
                }

            }
    }

    @Throws(IOException::class)
    @After
    fun stopServer() {
        mockWebServer.shutdown()
    }

    @Throws(IOException::class)
    fun enqueueResponse(fileName: String) {
        enqueueResponse(fileName, emptyMap())
    }

    @Throws(IOException::class)
    private fun enqueueResponse(fileName: String, headers: Map<String, String>) {
        val inputStream = javaClass.classLoader?.getResourceAsStream("api-response/$fileName")
        val source = inputStream?.source()?.buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        source?.readString(StandardCharsets.UTF_8)?.let { content ->
            mockWebServer.enqueue(mockResponse.setBody(content))
        }
    }

    fun createService(c: Class<T>): T {
        return Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(c)
    }


    companion object {
        @Parameterized.Parameters
        @JvmStatic
        fun param(): List<Boolean> {
            return arrayListOf(true, true)
        }
    }
}
