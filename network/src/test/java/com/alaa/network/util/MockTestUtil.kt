package com.alaa.network.util

import com.alaa.entity.entities.User


object MockTestUtil {

    private fun mockUser() =
        User(
            2,
            "janet.weaver@reqres.in",
            "Janet",
            "Weaver",
            "https://reqres.in/img/faces/2-image.jpg"
        )

    fun mockUsersList() = listOf(mockUser())
}
