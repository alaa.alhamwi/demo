package com.alaa.network.util

import com.alaa.network.vo.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


class TestCollector<T> {
    private val values = mutableListOf<Resource<T>>()
    private val valuesToCheck = mutableListOf<Resource<T>>()

    lateinit var assertBlock: (responseValues: MutableList<Resource<T>>, assertedValues: MutableList<Resource<T>>) -> Unit

    @Synchronized
    fun test(
        scope: CoroutineScope,
        flow: Flow<Resource<T>>,
        block: (responseValues: MutableList<Resource<T>>, assertedValues: MutableList<Resource<T>>) -> Unit
    ): Job {
        this.assertBlock = block
        return scope.launch {
            flow.collect {
                values.add(it)
            }
            assertBlock(values, valuesToCheck)
        }
    }

    fun assertValues(vararg _values: Resource<T>) {
        valuesToCheck.addAll(_values)
    }


}