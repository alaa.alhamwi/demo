package com.alaa.network

import com.alaa.entity.entities.User
import com.alaa.network.service.UserTestService
import com.alaa.network.util.ApiAbstract
import com.alaa.network.util.MainCoroutinesRule
import com.alaa.network.util.MockTestUtil
import com.alaa.network.util.TestCollector
import com.alaa.network.vo.Resource
import com.alaa.network.vo.Status
import com.alaa.network.vo.response.GetUserResponse
import com.alaa.network.vo.response.util.ApiResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import kotlin.test.assertEquals

@RunWith(Parameterized::class)
class UserServiceTest(private val fetchFromNetwork: Boolean) :
    ApiAbstract<UserTestService, List<User>, GetUserResponse>() {

    private lateinit var service: UserTestService

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesRule = MainCoroutinesRule()

    @Before
    fun initService() {
        service = createService(UserTestService::class.java)
    }

    @Before
    fun init() {
        enqueueResponse("/Users.json")
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `test service`() = runBlockingTest {
        handleShouldFetch = { fetchFromNetwork }

        var fetchedDbValue = MockTestUtil.mockUsersList()

        handleSaveCallResult = { it ->
            fetchedDbValue = it.users
        }

        handleLoadFromDb = {
            MockTestUtil.mockUsersList()
        }

        handleCreateCall = {
            flow {
                val responseBody = requireNotNull(service.fetchUsers())

                emit(ApiResponse.create(responseBody))
            }
        }
        val collector = TestCollector<List<User>>()
        val job = collector.test(
            this,
            networkBoundResource.asFlowData(),
            block = { responseValues, assertedValues ->
                assert(responseValues.size == 3)
                assert(assertedValues.size == 2)
                assert(responseValues[1].data?.size == assertedValues[1].data?.size)
                assert(responseValues[2].data != null)
                assert(responseValues[2].data!!.isNotEmpty())
            })

        collector.assertValues(
            Resource.loading(null), Resource.success(fetchedDbValue)
        )
        job.cancel()
    }
}
