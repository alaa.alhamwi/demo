package com.alaa.network.service

import com.alaa.network.constants.ConnectionEndPoint
import com.alaa.network.vo.response.GetUserResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * REST API access points
 */
interface UserTestService {

    @GET(ConnectionEndPoint.RECENT_USERS)
    suspend fun fetchUsers(): Response<GetUserResponse>

  }