package com.alaa.network


import androidx.annotation.VisibleForTesting
import androidx.test.espresso.idling.CountingIdlingResource
import java.lang.reflect.Modifier
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.timerTask

/**
 * Handles the changes to the [CountingIdlingResource] being used during Espresso test.
 */
object IdlingResourceHelper {

    /** Indicates whether the app is running an Espresso test */
    private var isRunningTest: AtomicBoolean? = null

    /** The idling resource used to indicate async operations during tests. */
    private val resource = CountingIdlingResource(IdlingResourceHelper::class.java.name)

    /**
     * Increments [resource]. This is only done while in [BuildConfig.DEBUG] and [isRunningTest].
     * Otherwise, it does nothing.
     */
    fun increment() {
        if (BuildConfig.DEBUG && isRunningTest())
            resource.increment()
    }

    /**
     * Decrements [resource]. This is only done while in [BuildConfig.DEBUG] and [isRunningTest].
     * Otherwise, it does nothing.
     */
    fun decrement() {
        if (BuildConfig.DEBUG && isRunningTest())
            resource.decrement()
    }

    /**
     * Calls [decrement] after a certain [delay]. The goal is to give the UI a bit of time to handle
     * the response before decrementing the [resource].
     */
    fun decrementWithDelay(delay: Long = 500) {
        Timer().schedule(timerTask {
            decrement()
        }, delay)
    }

    /**
     *  @return the [resource] to be used for registration. This should only be accessed while
     *  running a test.
     */
    @VisibleForTesting(otherwise = Modifier.PRIVATE)
    fun getIdlingResource(): CountingIdlingResource = resource

    /**
     * @return a flag indicating whether the app is running an Espresso test.
     */
    @Synchronized
    fun isRunningTest(): Boolean {
        if (null == isRunningTest) {
            val isTest: Boolean = try {

                Class.forName("androidx.test.espresso.Espresso")
                true
            } catch (e: ClassNotFoundException) {
                false
            }

            isRunningTest = AtomicBoolean(isTest)
        }

        return isRunningTest?.get() == true
    }


}