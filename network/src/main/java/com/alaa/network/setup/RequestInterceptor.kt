package com.alaa.network.setup

import com.alaa.network.services.AuthTokenService
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Response


class RequestInterceptor : Interceptor {

    private val authTokenService = AuthTokenService()

    override fun intercept(chain: Interceptor.Chain): Response {
        var originalRequest = chain.request()

        val url: HttpUrl = originalRequest.url.newBuilder()
            .build()
        originalRequest = originalRequest.newBuilder().url(url).build()


        val requestBuilder = originalRequest.newBuilder()

        //Add auth token
        authTokenService.authToken.let {
            if (it.isNotBlank()) requestBuilder.addHeader("Authorization", it)
        }

        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}
