package com.alaa.network.vo.response.util

interface DataResponse<T : Any> {

    /**
     * Response data can be converted into [T] via this method.
     */
    fun toModel(): T
}