package com.alaa.network.vo

import com.alaa.network.vo.Status.*
import java.net.HttpURLConnection.HTTP_OK

/**
 * A generic class that holds a value with its loading status.
 * @param <T>
</T> */
class Resource<out T>(
    val status: Status,
    val data: T?,
    val message: String?,
    val code: Int = -1,
) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(SUCCESS, data, null, HTTP_OK)
        }

        fun <T> error(msg: String, data: T?, code: Int, rawResponse: String? = null): Resource<T> {
            return Resource(ERROR, data, msg, code)
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(LOADING, data, null, -1)
        }
    }

    override fun equals(other: Any?): Boolean {

        if (other is Resource<*>) {
            if (other.status == this.status) {
                return true
            }
        }

        return super.equals(other)
    }

    fun dataIsNull() = data == null
}