package com.alaa.network.vo.response

import com.alaa.entity.entities.User
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class GetUserResponse(
    @Json(name = "per_page")
    val perPage: Int,
    val page: Int,
    val total: Int,
    @Json(name = "total_pages")
    val totalPages: Int,
    @Json(name = "data")
    val users: MutableList<User>
)