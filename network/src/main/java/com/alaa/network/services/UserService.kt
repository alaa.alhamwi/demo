package com.alaa.network.services

import com.alaa.network.constants.ConnectionEndPoint
import com.alaa.network.vo.response.GetUserResponse
import com.alaa.network.vo.response.util.ApiResponse
import retrofit2.http.GET

/**
 * REST API access points
 */
interface UserService {

    @GET(ConnectionEndPoint.RECENT_USERS)
    suspend fun fetchRecentUsers(): ApiResponse<GetUserResponse>

}