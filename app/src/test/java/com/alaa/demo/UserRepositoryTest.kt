package com.alaa.demo


import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.alaa.demo.domains.UserDomain
import com.alaa.demo.repository.UserRepository
import com.alaa.demo.util.MainCoroutinesRule
import com.alaa.entity.entities.User
import com.alaa.network.vo.Resource
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations
import kotlin.test.assertEquals


class UserRepositoryTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesRule = MainCoroutinesRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private val userDomain: UserDomain = mock()
    private lateinit var repository: UserRepository


    @Before
    fun setup() {
        repository =
            UserRepository(userDomain)
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testUsers() = runBlocking {
        val loadState = Resource.loading(null)
        val dataState = Resource.success(arrayListOf<User>())

        whenever(userDomain.fetchRecentUsers()).thenReturn(flow {
            emit(loadState)
            emit(dataState)
        })

        val flow = repository.getRecentUsers()

        val listCollect = arrayListOf<Resource<ArrayList<User>>>()
        flow.collect {
            listCollect.add(it)
        }

        //verify that the first result is the mocked loading state
        assertEquals(listCollect[0], loadState)
        //verify that the first result is the mocked success result
        assertEquals(listCollect[1], dataState)

    }


}