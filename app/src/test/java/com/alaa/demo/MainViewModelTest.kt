package com.alaa.demo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.alaa.demo.repository.UserRepository
import com.alaa.demo.ui.home.viewmodel.MainViewModel
import com.alaa.demo.util.MainCoroutinesRule
import com.alaa.entity.entities.User
import com.alaa.network.vo.Resource
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class UserViewModelTest {


    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesRule = MainCoroutinesRule()

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainViewModel
    private val repository: UserRepository = mock()

    @Mock
    private lateinit var observer: Observer<Resource<ArrayList<User>>>

    @Before
    fun setup() {
        viewModel =
            MainViewModel(repository)
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun getUserTest() = runBlocking {

        val loadState = Resource.loading(null)
        val dataState = Resource.success(arrayListOf<User>())

        whenever(repository.getRecentUsers()).thenReturn(flow {
            emit(loadState)
            emit(dataState)
        })

        viewModel.usersResourceLiveData.observeForever(observer)


        viewModel.getUsers()

        verify(observer).onChanged(loadState)
        verify(observer).onChanged(dataState)
        viewModel.usersResourceLiveData.removeObserver(observer)

    }

    // test get users with error response
    @Test
    fun getUsersTestWithError() = runBlocking {

        //mock loading status
        val loadState = Resource.loading(null)
        //mock error response
        val dataState = Resource.error("Error", null, 0)

        //each time calling getRecentUsers() then don'r return its flow, and return the given flow instead
        whenever(repository.getRecentUsers()).thenReturn(flow {
            //emit loading Resource
            emit(loadState)
            //emit error response
            emit(dataState)
        })

        //observe the live data which will receive the response
        viewModel.usersResourceLiveData.observeForever(observer)

        //ask to get the users
        viewModel.getUsers()

        //verify that the first received state is a loading state
        verify(observer).onChanged(loadState)
        //verify that the second received state is a data state "error"
        verify(observer).onChanged(dataState)

        //remove the observer
        viewModel.usersResourceLiveData.removeObserver(observer)
    }


}
