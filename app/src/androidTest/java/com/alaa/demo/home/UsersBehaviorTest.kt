package com.alaa.demo.home

import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.alaa.demo.BaseBehaviorTest
import com.alaa.demo.R
import com.alaa.demo.base.BaseActivity
import com.alaa.demo.ui.home.MainActivity
import com.alaa.demo.ui.home.fragment.UsersFragment
import com.alaa.demo.ui.home.viewholder.UserViewHolder
import com.alaa.demo.utils.IdlingResourceRule
import org.hamcrest.Matchers
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.junit.runner.RunWith

/**
 * Instrumentation test for the behavior of the [UsersFragment] in the [MainActivity]
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class UsersBehaviorTest : BaseBehaviorTest() {

    /** [TestRule] used to manage [IdlingResource] registration before/after running test cases */
    @get:Rule
    val idlingResourceRule = IdlingResourceRule()


    @Test
    fun test_users_list() {
        val currentActivity = getCurrentActivity() as BaseActivity<*>?

        // If the current activity isn't MainActivity then fail the test
        if (currentActivity is MainActivity) {
            val navController = currentActivity.navController

            // Make sure the navController moved to the usersFragment
            assertEquals(navController.currentDestination?.id, R.id.usersFragment)

            //wait for the network request to be done
            Thread.sleep(3000)

            // check the progress bar  is not displayed after getting the network response
            Espresso.onView(
                Matchers.allOf(
                    withId(R.id.progressBarr),
                    ViewMatchers.isDescendantOfA(withId(R.id.parentCL))
                )
            ).check(
                ViewAssertions.matches(ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.GONE))
            )
            //check if users recycler is visible
            Espresso.onView(
                Matchers.allOf(
                    withId(R.id.usersRV),
                    ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                )
            ).check(ViewAssertions.matches(ViewMatchers.isCompletelyDisplayed()))

        } else {
            fail("${MainActivity::class.java.name} should be launched, but ${currentActivity?.localClassName} was launched.")
        }
    }


    @Test
    fun test_user_details() {
        val currentActivity = getCurrentActivity() as BaseActivity<*>?

        // If the current activity isn't MainActivity then fail the test
        if (currentActivity is MainActivity) {
            val navController = currentActivity.navController

            test_users_list()

            //check if users recycler is visible and clicks on the first item
            Espresso.onView(
                Matchers.allOf(
                    withId(R.id.usersRV),
                    ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
                )
            )
                .check(ViewAssertions.matches(ViewMatchers.isCompletelyDisplayed()))
                .perform(
                    RecyclerViewActions.actionOnItemAtPosition<UserViewHolder>(
                        0,
                        ViewActions.click()
                    )
                )


            // Make sure the navController moved to the userDetailsFragment
            assertEquals(navController.currentDestination?.id, R.id.userDetailsFragment)

            //wait for the user details page to be opened
            Thread.sleep(500)

            //empty view in support fragment because the user is not logged in
            Espresso.onView(withId(R.id.userAvatarIV))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

            Espresso.onView(withId(R.id.userNameTV))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        }

    }

}

