package com.alaa.demo

import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner

import org.junit.Test
import org.junit.runner.RunWith


/**
 * Dummy instrumented test, which will execute on an Android device.
 */
@RunWith(AndroidJUnit4ClassRunner::class)
class DummyBehaviorTest : BaseBehaviorTest() {


    @Test
    fun dummyTest() {

    }
}

