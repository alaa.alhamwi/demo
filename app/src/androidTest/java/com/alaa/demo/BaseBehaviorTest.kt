package com.alaa.demo

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.StringRes
import androidx.appcompat.widget.SwitchCompat
import androidx.test.espresso.*
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.matcher.IntentMatchers
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.lifecycle.ActivityLifecycleMonitorRegistry
import androidx.test.runner.lifecycle.Stage
import com.alaa.demo.base.BaseActivity
import com.alaa.demo.ui.home.MainActivity
import org.hamcrest.*
import org.hamcrest.CoreMatchers.isA
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.koin.test.KoinTest
import java.util.concurrent.TimeUnit


open class BaseBehaviorTest : KoinTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java, false, false)


    /**
     * Setups what needed to happen before starting every test (e.g. clearing user data,
     * initializing Intents, increasing idling timeout...etc). May be overridden for a custom
     * behavior.
     */
    @Before
    open fun before() {
        Intents.init()

        // Clear data storage
        clearDB()

        // Increase idling timeout to avoid network delays.
        IdlingPolicies.setIdlingResourceTimeout(5, TimeUnit.MINUTES)
        // Launch activity manually
        mActivityTestRule.launchActivity(null)

    }

    /**
     * Setups what needed to happen after finishing every test (e.g. releasing Intents).
     * May be overridden for a custom behavior.
     */
    @After
    open fun after() {
        Intents.release()
    }

    /**
     * Gets the current Activity being displayed (the most recently resumed activity)
     *
     * @throws Throwable
     * @return currently being displayed Activity
     */
    @Throws(Throwable::class)
    fun getCurrentActivity(): Activity? {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        val activity = arrayOfNulls<Activity>(1)
        mActivityTestRule.runOnUiThread {
            val activities =
                ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED)
            activity[0] = Iterables.getOnlyElement(activities)
        }
        return activity[0]
    }

    /**
     * Gets the string that matches given string resource ID
     *
     * @param resId: resource ID for the wanted string
     * @return the string matching the given resId
     */
    fun getResourceString(@StringRes resId: Int): String {
        val targetContext = InstrumentationRegistry.getInstrumentation().targetContext
        return targetContext.resources.getString(resId)
    }


    /**
     * clear local db
     */
    private fun clearDB() {
        InstrumentationRegistry.getInstrumentation().targetContext.deleteDatabase(
            getResourceString(R.string.app_name)
        )
    }

    fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }

    /**
     * preform click on specific view with given id
     */
    fun clickOnViewChild(viewId: Int) = object : ViewAction {
        override fun getConstraints() = null

        override fun getDescription() = "Click on a child view with specified id."

        override fun perform(uiController: UiController, view: View) =
            click().perform(uiController, view.findViewById<View>(viewId))
    }

    /**
     * perform click on recyclerview item
     */
    fun recyclerClick() = object : ViewAction {
        override fun getConstraints() = null

        override fun getDescription() = "Perform click on recyclerview item."

        override fun perform(uiController: UiController, view: View) =
            click().perform(uiController, view)
    }

    /**
     * gets string displayed in a textview
     */

    class GetTextAction(private val viewId: Int) : ViewAction {
        private var text: CharSequence? = null

        override fun getConstraints() = null

        override fun getDescription() = "get text"

        override fun perform(
            uiController: UiController?,
            view: View
        ) {
            val textView = view.findViewById<View>(viewId) as TextView
            text = textView.text
        }

        fun getText(): CharSequence? {
            return text
        }
    }

    fun <T> first(matcher: Matcher<T>): Matcher<T> {
        return object : BaseMatcher<T>() {
            internal var isFirst = true
            override fun matches(item: Any): Boolean {
                if (isFirst && matcher.matches(item)) {
                    isFirst = false
                    return true
                }
                return false
            }

            override fun describeTo(description: Description) {
                description.appendText("should return first matching item")
            }
        }
    }


    /**
     * switch on / off swipe compat view
     * @param checked : swipe compat value
     */
    fun setSwitchCompat(checked: Boolean = false) = object : ViewAction {
        val checkableViewMatcher = object : BaseMatcher<View>() {
            override fun matches(item: Any?): Boolean = isA(SwitchCompat::class.java).matches(item)

            override fun describeTo(description: Description?) {
                description?.appendText("is Checkable instance ")
            }
        }


        override fun getConstraints(): BaseMatcher<View> = checkableViewMatcher
        override fun getDescription(): String? = null
        override fun perform(uiController: UiController?, view: View) {
            val scView: SwitchCompat = view as SwitchCompat
            scView.isChecked = checked
        }
    }

    fun isViewDisplayed(viewInteraction: ViewInteraction): Boolean {
        return try {
            viewInteraction.check(matches(ViewMatchers.isDisplayed()))
            true
        } catch (e: NoMatchingViewException) {
            false
        }
    }

}