package com.alaa.demo.utils

import com.alaa.network.IdlingResourceHelper
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

/**
 *  [TestRule] for managing [IdlingResource] registration before/after
 *  running a test case.
 *  @see IdlingResourceHelper
 */
class IdlingResourceRule : TestRule {

    /**
     * Idling resource that will be registered before/after running a test case
     * @see IdlingResourceHelper.getIdlingResource
     */
    private val resource: IdlingResource = IdlingResourceHelper.getIdlingResource()

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            override fun evaluate() {
                IdlingRegistry.getInstance().register(resource)
                base.evaluate()
                IdlingRegistry.getInstance().unregister(resource)
            }
        }
    }
}