package com.alaa.demo.di

import com.alaa.demo.domains.UserDomain
import org.koin.dsl.module

/*
viewModelModule is responsible for injecting domains
 */
val domainModule = module {
    single { UserDomain(get(), get()) }
}
