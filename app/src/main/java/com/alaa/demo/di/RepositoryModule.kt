package com.alaa.demo.di

import com.alaa.demo.repository.UserRepository
import org.koin.dsl.module
/*
repositoryModule is responsible for injecting repositories
 */
val repositoryModule = module {
    single { UserRepository(get()) }
}
