package com.alaa.demo.di

import androidx.room.Room
import com.alaa.entity.database.AppDatabase
import com.squareup.moshi.Moshi
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

/*
networkModule is responsible for network requests
 */
val persistenceModule = module {

    single {
        Room.databaseBuilder(
            androidApplication(), AppDatabase::class.java,
            "DB name"
        )
            .allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    single {
        Moshi.Builder().build()
    }


    single { get<AppDatabase>().userDao() }
}