package com.alaa.demo.di


import com.alaa.demo.ui.home.viewmodel.MainViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

/*
viewModelModule is responsible for injecting view models
 */
val viewModelModule = module {
    viewModel {
        MainViewModel(
            get()
        )
    }
}
