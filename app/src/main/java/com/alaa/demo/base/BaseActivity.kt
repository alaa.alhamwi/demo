package com.alaa.demo.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<T : BaseViewModel> : AppCompatActivity() {

    private var mViewModel: T? = null

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getActivityViewModel(): T?


    /**
     * @param resId layout resource id
     * @return Data binding
     */
    protected inline fun <reified T : ViewDataBinding> binding(
        @LayoutRes resId: Int
    ): Lazy<T> = lazy { DataBindingUtil.setContentView<T>(this, resId) }


    /**
     * Override to initialize activity layout
     *
     */
    open fun initializeUI() {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUp()
        initializeUI()
    }

    private fun setUp() {
        mViewModel = mViewModel?.let { it } ?: getActivityViewModel()
    }


}
