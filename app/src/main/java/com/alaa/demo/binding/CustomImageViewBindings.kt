package com.alaa.demo.binding

import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.alaa.demo.R
import com.bumptech.glide.Glide

@BindingAdapter("image")
fun bindImage(view: ImageView, path: String?) {
    path?.let {
        Glide.with(view.context).load(path)
            .placeholder(ContextCompat.getDrawable(view.context, R.drawable.ic_profile_placeholder))
            .into(view)
    }
}