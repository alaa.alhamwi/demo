package com.alaa.demo.binding

import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import com.alaa.commonui.extension.gone
import com.alaa.commonui.extension.visible
import com.alaa.network.vo.Status

@BindingAdapter("stateLoader", "noCachedData")
fun bindLoaderState(view: ProgressBar, status: Status?, noCachedData: Boolean?) {
    when (status) {
        Status.LOADING -> {
            //show the progress bar while loading the data
            view.visible()
        }
        Status.ERROR -> {
            //hide the progress bar once getting a  network response with an error
            view.gone()
        }
        else -> {
            //show the the progress bar once getting a network response with  unempty data
            if (noCachedData == true)
                view.visible()
            else
                view.gone()
        }
    }
}