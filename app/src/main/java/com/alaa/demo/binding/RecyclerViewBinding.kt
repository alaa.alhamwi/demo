package com.alaa.demo.binding

import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.alaa.demo.ui.home.adapter.UserAdapter
import com.alaa.entity.entities.User
import com.alaa.network.vo.Resource
import com.skydoves.baserecyclerviewadapter.BaseAdapter

@BindingAdapter("adapter")
fun bindAdapter(view: RecyclerView, baseAdapter: BaseAdapter) {
    view.adapter = baseAdapter
}

@BindingAdapter("layoutManager")
fun bindLayoutManager(view: RecyclerView, layoutManager: RecyclerView.LayoutManager) {
    view.layoutManager = layoutManager
}

@BindingAdapter("adapterUserList")
fun bindAdapterUserList(view: RecyclerView, users: Resource<List<User>?>?) {
    users?.data?.let {
        if (it.isNotEmpty())
            (view.adapter as? UserAdapter)?.addUserList(it)
    }
}
