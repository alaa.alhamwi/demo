package com.alaa.demo.binding

import android.util.Log
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.alaa.commonui.extension.gone
import com.alaa.commonui.extension.visible
import com.alaa.network.vo.Status

@BindingAdapter("stateError", "noCachedData")
fun bindErrorState(view: TextView, status: Status?, noCachedData: Boolean?) {
    //show the error text view only when receiving and error and there no cached data
    when (status) {
        Status.ERROR -> {
            if (noCachedData == true)
                view.visible()
            else
                view.gone()
        }
        else -> {
            view.gone()
        }
    }
}