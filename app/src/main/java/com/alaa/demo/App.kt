package com.alaa.demo

import com.alaa.demo.di.*
import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        //initialize the dependency injection
        //used Koin
        startKoin {
            androidContext(this@App)
            modules(
                listOf(
                    networkModule,
                    persistenceModule,
                    repositoryModule,
                    domainModule,
                    viewModelModule)
            )
        }
    }
}