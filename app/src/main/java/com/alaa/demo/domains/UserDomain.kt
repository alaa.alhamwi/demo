package com.alaa.demo.domains

import com.alaa.entity.database.UserDao
import com.alaa.entity.entities.User
import com.alaa.network.resources.NetworkBoundResource
import com.alaa.network.services.UserService
import com.alaa.network.vo.Resource
import com.alaa.network.vo.response.GetUserResponse
import com.alaa.network.vo.response.util.ApiResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserDomain constructor(val userService: UserService, val userDao: UserDao) {


    /*
        load users list from locale db & refresh its content from the restAPI
     */

    fun fetchRecentUsers(): Flow<Resource<ArrayList<User>>> {
        return object :
        // ResultType: Type for the Resource data.
        // RequestType: Type for the API response.
            NetworkBoundResource<ArrayList<User>, GetUserResponse>() {

            //indicate if the returned users are old or fresh users
            var isCached = true

            // Called when the fetch fails.
            override fun onFetchFailed() {
            }

            // Called to get the cached data from the database.
            override suspend fun loadFromDb(): ArrayList<User> {
                val cachedItems = ArrayList(userDao.getUsers())
                //remove the avatar when the user is a cached on in order to show only updated avatars
                if (isCached)
                    for (item in cachedItems) {
                        item.avatar = ""
                    }
                return cachedItems
            }

            // Called to save the result of the API response into the database
            override suspend fun saveCallResult(item: GetUserResponse) {
                //save the network response "users" in the locale db
                userDao.setUsers(item.users)
            }

            // Called with the data in the database to decide whether to fetch
            // potentially updated data from the network.
            override fun shouldFetch(data: ArrayList<User>?) = true

            // Called to create the API call.
            override suspend fun createCall(): Flow<ApiResponse<GetUserResponse>?> {
                return flow {
                    //mark the data as updated data
                    isCached = false
                    emit(userService.fetchRecentUsers())
                }
            }

        }.asFlowData()
    }


}