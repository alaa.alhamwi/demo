package com.alaa.demo.repository

import com.alaa.demo.domains.UserDomain
import com.alaa.entity.database.UserDao
import com.alaa.entity.entities.User
import com.alaa.network.vo.Resource
import kotlinx.coroutines.flow.Flow

class UserRepository constructor(
    private val userDomain: UserDomain
) {

   /*
    load users list from locale db & refresh its content from the restAPI
    */
    fun getRecentUsers(): Flow<Resource<ArrayList<User>>> {
        return userDomain.fetchRecentUsers()
    }

}
