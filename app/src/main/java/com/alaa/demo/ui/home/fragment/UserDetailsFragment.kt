package com.alaa.demo.ui.home.fragment

import com.alaa.demo.R
import com.alaa.demo.base.BaseFragment
import com.alaa.demo.databinding.FragmentUserDetailsBinding
import com.alaa.demo.ui.home.viewmodel.MainViewModel
import com.alaa.entity.entities.User
import org.koin.android.viewmodel.ext.android.viewModel

class UserDetailsFragment : BaseFragment<MainViewModel, FragmentUserDetailsBinding>() {

    private val viewModel by viewModel<MainViewModel>()

    val user: User? by lazy {
        arguments?.getParcelable(USER_DETAILS)
    }

    override fun getFragmentViewModel(): MainViewModel? {
        return viewModel
    }

    companion object {
        const val USER_DETAILS = "USER_DETAILS"
    }

    override fun getLayoutId() = R.layout.fragment_user_details

    override fun initializeUI() {
        super.initializeUI()

        binding.apply {
            user = this@UserDetailsFragment.user
            lifecycleOwner = this@UserDetailsFragment
        }
    }


}