package com.alaa.demo.ui.home

import androidx.annotation.VisibleForTesting
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.alaa.demo.R
import com.alaa.demo.base.BaseActivity
import com.alaa.demo.databinding.ActivityMainBinding
import com.alaa.demo.ui.home.viewmodel.MainViewModel
import org.koin.android.viewmodel.ext.android.viewModel
import java.lang.reflect.Modifier


class MainActivity : BaseActivity<MainViewModel>() {

    private val viewModel by viewModel<MainViewModel>()
    private val binding: ActivityMainBinding by binding(R.layout.activity_main)

    @VisibleForTesting(otherwise = Modifier.PRIVATE)
    lateinit var navController: NavController

    override fun getActivityViewModel(): MainViewModel {
        return viewModel
    }

    override fun initializeUI() {
        super.initializeUI()

        binding.apply {
            lifecycleOwner = this@MainActivity
        }

        navController = findNavController(R.id.usersNavHostFragment)

    }

}