package com.alaa.demo.ui.home.adapter

import android.view.View
import com.alaa.demo.R
import com.alaa.demo.ui.home.viewholder.UserViewHolder
import com.alaa.entity.entities.User
import com.skydoves.baserecyclerviewadapter.BaseAdapter
import com.skydoves.baserecyclerviewadapter.SectionRow


class UserAdapter(
    private val delegate: UserViewHolder.Delegate
) : BaseAdapter(
) {

    init {
        addSection(arrayListOf<User>())
    }

    fun addUserList(users: List<User>) {
        sections().first().run {
            //remove old items
            clear()
            //add the new items
            addAll(users)
            //notify the recycler about its data change
            notifyDataSetChanged()
        }
    }

    override fun layout(sectionRow: SectionRow) = R.layout.user_item_layout

    override fun viewHolder(layout: Int, view: View) = UserViewHolder(view, delegate)

}