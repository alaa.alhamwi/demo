package com.alaa.demo.ui.home.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.alaa.demo.base.BaseViewModel
import com.alaa.demo.repository.UserRepository
import com.alaa.entity.entities.User
import com.alaa.network.vo.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MainViewModel constructor(
    private val userRepository: UserRepository,
    private val context: CoroutineContext = Dispatchers.Main
) : BaseViewModel() {

    //users list
    var usersResourceLiveData: MutableLiveData<Resource<ArrayList<User>>> = MutableLiveData(
    )

    /*
    load the users
     */
    fun getUsers() {
        viewModelScope.launch(context = context) {
            collect(userRepository.getRecentUsers()) {
                //save the users result in order to render them
                usersResourceLiveData.value = it
            }

        }
    }

}