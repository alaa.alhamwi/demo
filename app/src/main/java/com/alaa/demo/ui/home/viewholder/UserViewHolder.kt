package com.alaa.demo.ui.home.viewholder

import android.view.View
import com.alaa.demo.binding.bindings
import com.alaa.demo.databinding.UserItemLayoutBinding
import com.alaa.entity.entities.User
import com.skydoves.baserecyclerviewadapter.BaseViewHolder

class UserViewHolder(
    view: View,
    private val delegate: Delegate
) : BaseViewHolder(view) {

    // an interface that is responsible for the action that should be taken
    // on each user cell click
    interface Delegate {
        fun onItemClick(user: User, view: View)
    }

    //user to be added to the cell
    lateinit var user: User

    //user layout
    private val binding by bindings<UserItemLayoutBinding>(
        view
    )

    override fun bindData(data: Any) {
        if (data is User) {
            user = data
            //add the user as a binding variable in order to render it
            binding.user = user
            binding.executePendingBindings()
        }
    }

    //on cell "ser" click, invoke the delegate action
    override fun onClick(p0: View?) =
        delegate.onItemClick(user, itemView.rootView)

    override fun onLongClick(v: View?): Boolean {
        return false
    }


}