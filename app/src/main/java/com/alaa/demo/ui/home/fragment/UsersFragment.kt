package com.alaa.demo.ui.home.fragment

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.alaa.demo.R
import com.alaa.demo.base.BaseFragment
import com.alaa.demo.databinding.FragmentUsersBinding
import com.alaa.demo.ui.home.adapter.UserAdapter
import com.alaa.demo.ui.home.viewholder.UserViewHolder
import com.alaa.demo.ui.home.viewmodel.MainViewModel
import com.alaa.entity.entities.User
import org.koin.android.viewmodel.ext.android.viewModel
import java.util.*

class UsersFragment : BaseFragment<MainViewModel, FragmentUsersBinding>(), UserViewHolder.Delegate {

    private val viewModel by viewModel<MainViewModel>()
    private val usersAdapter = UserAdapter(this@UsersFragment)

    override fun getFragmentViewModel(): MainViewModel? {
        return viewModel
    }

    override fun getLayoutId() = R.layout.fragment_users

    override fun initializeUI() {
        super.initializeUI()


        binding.apply {
            viewModel = this@UsersFragment.viewModel
            lifecycleOwner = this@UsersFragment
            layoutManager = LinearLayoutManager(activity)
            adapter = usersAdapter
        }

        //load the users
        viewModel.getUsers()
    }

    override fun onItemClick(user: User, view: View) {
        //open user details fragment
        //pass the clicked user object to the new fragment through its bundle
        val args = Bundle()
        args.putParcelable(UserDetailsFragment.USER_DETAILS, user)

        findNavController().navigate(R.id.next_action_user_details, args)
    }
}